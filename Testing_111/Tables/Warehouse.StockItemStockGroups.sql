SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [Warehouse].[StockItemStockGroups] (
		[StockItemStockGroupID]     [int] NOT NULL,
		[StockItemID]               [int] NOT NULL,
		[StockGroupID]              [int] NOT NULL,
		[LastEditedBy]              [int] NOT NULL,
		[LastEditedWhen]            [datetime2](7) NOT NULL,
		CONSTRAINT [UQ_StockItemStockGroups_StockItemID_Lookup]
		UNIQUE
		NONCLUSTERED
		([StockItemID], [StockGroupID])
		ON [USERDATA],
		CONSTRAINT [UQ_StockItemStockGroups_StockGroupID_Lookup]
		UNIQUE
		NONCLUSTERED
		([StockGroupID], [StockItemID])
		ON [USERDATA],
		CONSTRAINT [PK_Warehouse_StockItemStockGroups]
		PRIMARY KEY
		CLUSTERED
		([StockItemStockGroupID])
	ON [USERDATA]
)
GO
ALTER TABLE [Warehouse].[StockItemStockGroups]
	ADD
	CONSTRAINT [DF_Warehouse_StockItemStockGroups_StockItemStockGroupID]
	DEFAULT (NEXT VALUE FOR [Sequences].[StockItemStockGroupID]) FOR [StockItemStockGroupID]
GO
ALTER TABLE [Warehouse].[StockItemStockGroups]
	ADD
	CONSTRAINT [DF_Warehouse_StockItemStockGroups_LastEditedWhen]
	DEFAULT (sysdatetime()) FOR [LastEditedWhen]
GO
ALTER TABLE [Warehouse].[StockItemStockGroups]
	WITH CHECK
	ADD CONSTRAINT [FK_Warehouse_StockItemStockGroups_StockItemID_Warehouse_StockItems]
	FOREIGN KEY ([StockItemID]) REFERENCES [Warehouse].[StockItems] ([StockItemID])
ALTER TABLE [Warehouse].[StockItemStockGroups]
	CHECK CONSTRAINT [FK_Warehouse_StockItemStockGroups_StockItemID_Warehouse_StockItems]

GO
ALTER TABLE [Warehouse].[StockItemStockGroups]
	WITH CHECK
	ADD CONSTRAINT [FK_Warehouse_StockItemStockGroups_StockGroupID_Warehouse_StockGroups]
	FOREIGN KEY ([StockGroupID]) REFERENCES [Warehouse].[StockGroups] ([StockGroupID])
ALTER TABLE [Warehouse].[StockItemStockGroups]
	CHECK CONSTRAINT [FK_Warehouse_StockItemStockGroups_StockGroupID_Warehouse_StockGroups]

GO
ALTER TABLE [Warehouse].[StockItemStockGroups]
	WITH CHECK
	ADD CONSTRAINT [FK_Warehouse_StockItemStockGroups_Application_People]
	FOREIGN KEY ([LastEditedBy]) REFERENCES [Application].[People] ([PersonID])
ALTER TABLE [Warehouse].[StockItemStockGroups]
	CHECK CONSTRAINT [FK_Warehouse_StockItemStockGroups_Application_People]

GO
EXEC sp_addextendedproperty N'Description', N'Which stock items are in which stock groups', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemStockGroups', NULL, NULL
GO
EXEC sp_addextendedproperty N'Description', N'Internal reference for this linking row', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemStockGroups', 'COLUMN', N'StockItemStockGroupID'
GO
EXEC sp_addextendedproperty N'Description', N'Stock item assigned to this stock group (FK indexed via unique constraint)', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemStockGroups', 'COLUMN', N'StockItemID'
GO
EXEC sp_addextendedproperty N'Description', N'StockGroup assigned to this stock item (FK indexed via unique constraint)', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemStockGroups', 'COLUMN', N'StockGroupID'
GO
EXEC sp_addextendedproperty N'Description', N'Enforces uniqueness and indexes one side of the many to many relationship', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemStockGroups', 'CONSTRAINT', N'UQ_StockItemStockGroups_StockItemID_Lookup'
GO
EXEC sp_addextendedproperty N'Description', N'Enforces uniqueness and indexes one side of the many to many relationship', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemStockGroups', 'CONSTRAINT', N'UQ_StockItemStockGroups_StockGroupID_Lookup'
GO
ALTER TABLE [Warehouse].[StockItemStockGroups] SET (LOCK_ESCALATION = TABLE)
GO
