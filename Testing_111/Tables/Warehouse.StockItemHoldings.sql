SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [Warehouse].[StockItemHoldings] (
		[StockItemID]               [int] NOT NULL,
		[QuantityOnHand]            [int] NOT NULL,
		[BinLocation]               [nvarchar](20) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[LastStocktakeQuantity]     [int] NOT NULL,
		[LastCostPrice]             [decimal](18, 2) NOT NULL,
		[ReorderLevel]              [int] NOT NULL,
		[TargetStockLevel]          [int] NOT NULL,
		[LastEditedBy]              [int] NOT NULL,
		[LastEditedWhen]            [datetime2](7) NOT NULL,
		CONSTRAINT [PK_Warehouse_StockItemHoldings]
		PRIMARY KEY
		CLUSTERED
		([StockItemID])
	ON [USERDATA]
)
GO
ALTER TABLE [Warehouse].[StockItemHoldings]
	ADD
	CONSTRAINT [DF_Warehouse_StockItemHoldings_LastEditedWhen]
	DEFAULT (sysdatetime()) FOR [LastEditedWhen]
GO
ALTER TABLE [Warehouse].[StockItemHoldings]
	WITH CHECK
	ADD CONSTRAINT [PKFK_Warehouse_StockItemHoldings_StockItemID_Warehouse_StockItems]
	FOREIGN KEY ([StockItemID]) REFERENCES [Warehouse].[StockItems] ([StockItemID])
ALTER TABLE [Warehouse].[StockItemHoldings]
	CHECK CONSTRAINT [PKFK_Warehouse_StockItemHoldings_StockItemID_Warehouse_StockItems]

GO
ALTER TABLE [Warehouse].[StockItemHoldings]
	WITH CHECK
	ADD CONSTRAINT [FK_Warehouse_StockItemHoldings_Application_People]
	FOREIGN KEY ([LastEditedBy]) REFERENCES [Application].[People] ([PersonID])
ALTER TABLE [Warehouse].[StockItemHoldings]
	CHECK CONSTRAINT [FK_Warehouse_StockItemHoldings_Application_People]

GO
EXEC sp_addextendedproperty N'Description', N'Non-temporal attributes for stock items', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemHoldings', NULL, NULL
GO
EXEC sp_addextendedproperty N'Description', N'ID of the stock item that this holding relates to (this table holds non-temporal columns for stock)', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemHoldings', 'COLUMN', N'StockItemID'
GO
EXEC sp_addextendedproperty N'Description', N'Quantity currently on hand (if tracked)', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemHoldings', 'COLUMN', N'QuantityOnHand'
GO
EXEC sp_addextendedproperty N'Description', N'Bin location (ie location of this stock item within the depot)', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemHoldings', 'COLUMN', N'BinLocation'
GO
EXEC sp_addextendedproperty N'Description', N'Quantity at last stocktake (if tracked)', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemHoldings', 'COLUMN', N'LastStocktakeQuantity'
GO
EXEC sp_addextendedproperty N'Description', N'Unit cost price the last time this stock item was purchased', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemHoldings', 'COLUMN', N'LastCostPrice'
GO
EXEC sp_addextendedproperty N'Description', N'Quantity below which reordering should take place', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemHoldings', 'COLUMN', N'ReorderLevel'
GO
EXEC sp_addextendedproperty N'Description', N'Typical quantity ordered', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemHoldings', 'COLUMN', N'TargetStockLevel'
GO
ALTER TABLE [Warehouse].[StockItemHoldings] SET (LOCK_ESCALATION = TABLE)
GO
