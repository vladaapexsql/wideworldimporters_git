SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [Warehouse].[PackageTypes] (
		[PackageTypeID]       [int] NOT NULL,
		[PackageTypeName]     [nvarchar](50) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[LastEditedBy]        [int] NOT NULL,
		[ValidFrom]           [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
		[ValidTo]             [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
		PERIOD FOR SYSTEM_TIME ([ValidFrom], [ValidTo]),
		CONSTRAINT [UQ_Warehouse_PackageTypes_PackageTypeName]
		UNIQUE
		NONCLUSTERED
		([PackageTypeName])
		ON [USERDATA],
		CONSTRAINT [PK_Warehouse_PackageTypes]
		PRIMARY KEY
		CLUSTERED
		([PackageTypeID])
	ON [USERDATA]
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[Warehouse].[PackageTypes_Archive]))
GO
ALTER TABLE [Warehouse].[PackageTypes]
	ADD
	CONSTRAINT [DF_Warehouse_PackageTypes_PackageTypeID]
	DEFAULT (NEXT VALUE FOR [Sequences].[PackageTypeID]) FOR [PackageTypeID]
GO
ALTER TABLE [Warehouse].[PackageTypes]
	WITH CHECK
	ADD CONSTRAINT [FK_Warehouse_PackageTypes_Application_People]
	FOREIGN KEY ([LastEditedBy]) REFERENCES [Application].[People] ([PersonID])
ALTER TABLE [Warehouse].[PackageTypes]
	CHECK CONSTRAINT [FK_Warehouse_PackageTypes_Application_People]

GO
EXEC sp_addextendedproperty N'Description', N'Ways that stock items can be packaged (ie: each, box, carton, pallet, kg, etc.', 'SCHEMA', N'Warehouse', 'TABLE', N'PackageTypes', NULL, NULL
GO
EXEC sp_addextendedproperty N'Description', N'Numeric ID used for reference to a package type within the database', 'SCHEMA', N'Warehouse', 'TABLE', N'PackageTypes', 'COLUMN', N'PackageTypeID'
GO
EXEC sp_addextendedproperty N'Description', N'Full name of package types that stock items can be purchased in or sold in', 'SCHEMA', N'Warehouse', 'TABLE', N'PackageTypes', 'COLUMN', N'PackageTypeName'
GO
ALTER TABLE [Warehouse].[PackageTypes] SET (LOCK_ESCALATION = TABLE)
GO
