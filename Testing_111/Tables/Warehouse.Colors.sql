SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [Warehouse].[Colors] (
		[ColorID]          [int] NOT NULL,
		[ColorName]        [nvarchar](20) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[LastEditedBy]     [int] NOT NULL,
		[ValidFrom]        [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
		[ValidTo]          [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
		PERIOD FOR SYSTEM_TIME ([ValidFrom], [ValidTo]),
		CONSTRAINT [UQ_Warehouse_Colors_ColorName]
		UNIQUE
		NONCLUSTERED
		([ColorName])
		ON [USERDATA],
		CONSTRAINT [PK_Warehouse_Colors]
		PRIMARY KEY
		CLUSTERED
		([ColorID])
	ON [USERDATA]
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[Warehouse].[Colors_Archive]))
GO
ALTER TABLE [Warehouse].[Colors]
	ADD
	CONSTRAINT [DF_Warehouse_Colors_ColorID]
	DEFAULT (NEXT VALUE FOR [Sequences].[ColorID]) FOR [ColorID]
GO
ALTER TABLE [Warehouse].[Colors]
	WITH CHECK
	ADD CONSTRAINT [FK_Warehouse_Colors_Application_People]
	FOREIGN KEY ([LastEditedBy]) REFERENCES [Application].[People] ([PersonID])
ALTER TABLE [Warehouse].[Colors]
	CHECK CONSTRAINT [FK_Warehouse_Colors_Application_People]

GO
EXEC sp_addextendedproperty N'Description', N'Stock items can (optionally) have colors', 'SCHEMA', N'Warehouse', 'TABLE', N'Colors', NULL, NULL
GO
EXEC sp_addextendedproperty N'Description', N'Numeric ID used for reference to a color within the database', 'SCHEMA', N'Warehouse', 'TABLE', N'Colors', 'COLUMN', N'ColorID'
GO
EXEC sp_addextendedproperty N'Description', N'Full name of a color that can be used to describe stock items', 'SCHEMA', N'Warehouse', 'TABLE', N'Colors', 'COLUMN', N'ColorName'
GO
ALTER TABLE [Warehouse].[Colors] SET (LOCK_ESCALATION = TABLE)
GO
